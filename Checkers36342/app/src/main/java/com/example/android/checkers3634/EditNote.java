package com.example.android.checkers3634;

/**
 * Created by Shashank on 22-Oct-17.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.checkers3634.models.Note;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.R.attr.key;
import static com.example.android.checkers3634.R.string.username;
import static android.content.ContentValues.TAG;


public class EditNote extends AppCompatActivity {

    private Button btnCreate;
    private EditText etTitle, etContent;

    private DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_note);

        btnCreate = (Button) findViewById(R.id.new_note_btn);
        etContent = (EditText) findViewById(R.id.new_note_content);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("students");

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String content = etContent.getText().toString().trim();
                //createNote(content);
                writeStudentNote(content);
                Intent intent = new Intent(view.getContext(),Objectives.class);
                startActivity(intent);

            }
        });

        putData();
    }

    private void putData() {

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    if (snap.hasChild("notes")) {
                        String notes = snap.child("notes").child("content").getValue().toString();
                        etContent.setText(notes);

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.d(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    private void writeStudentNote(String content) {

        Note note = new Note(content);
        Map<String, Object> noteValues = note.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/" + Students.selectedStudentID + "/notes", noteValues);

        mDatabase.updateChildren(childUpdates);
    }

}









