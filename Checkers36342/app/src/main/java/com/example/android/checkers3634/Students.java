package com.example.android.checkers3634;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.android.checkers3634.models.Student;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;
import static com.example.android.checkers3634.Classes.selectedClass;

/**
 * Created by Shashank on 18-Oct-17.
 */

public class Students extends AppCompatActivity {

    private TextView progress1;
    private Button mButton1;
    private Button mButton2;
    private Button mButton3;
    private static final int REQUEST_CODE_CHEAT = 0;
    private int mProgress;
    private String progressString = Integer.toString(mProgress);
    private ListView Students;
    public static String selectedStudent;
    public static String selectedStudentID;
    public static Context mContext;
    private DatabaseReference mDatabase;
    private static ArrayList<Student> studentList;
    private MyCustomAdapter dataAdapter;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent
            data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_CODE_CHEAT) {
            if (data == null) {
                return;
            }
      //      mProgress = Objectives.progress(data);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.students);

        /*progress1 = (TextView) findViewById(R.id.progress1);
        progress1.setText(progressString + "/3");*/

        mContext = getApplicationContext();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("students");
        loadStudents();

        /*mButton1 = (Button) findViewById(R.id.button1);
        mButton1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
               // Intent intent = new Intent(v.getContext(),Objectives.class);
               // startActivityForResult(intent, REQUEST_CODE_CHEAT);
                int progress = mProgress;
          //      Intent intent = Objectives.newIntent(Students.this, progress);
           //     startActivityForResult(intent, REQUEST_CODE_CHEAT);

            }
        });
        mButton2 = (Button) findViewById(R.id.button2);
        mButton2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(),Objectives.class);
                startActivity(intent);
            }
        });
        mButton3 = (Button) findViewById(R.id.button3);
        mButton3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(),Objectives.class);
                startActivity(intent);
            }
        });*/
    }


    private void loadStudents() {

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                studentList = new ArrayList<Student>();
                Log.d(TAG, dataSnapshot.getChildrenCount() + "");
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    if (snap.hasChild("class-id") && snap.hasChild("zid") && snap.hasChild("first-name") && snap.hasChild("last-name")) {
                        String classID = snap.child("class-id").getValue().toString();
                        if (classID.equalsIgnoreCase(selectedClass)) {
                            String studentFirstName = snap.child("first-name").getValue().toString();
                            String studentLastName = snap.child("last-name").getValue().toString();
                            String studentID = snap.child("zid").getValue().toString();

                            Log.d(TAG, "Adding new student: " + snap.getValue().toString());
                            studentList.add(new Student(studentFirstName, studentLastName, studentID, classID));
                        }
                    }
                }
                populateView();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.d(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    private void populateView() {
        //create an ArrayAdaptar from the String Array
        dataAdapter = new MyCustomAdapter(mContext, R.layout.student_button, studentList);
        ListView listView = (ListView) findViewById(R.id.student_listview);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // When clicked, show a toast with the TextView text
                Student currStudent = (Student) parent.getItemAtPosition(position);

            }
        });
    }


    private class MyCustomAdapter extends ArrayAdapter<Student> {

        private ArrayList<Student> studentList;

        public MyCustomAdapter(Context context, int textViewResourceId,
                               ArrayList<Student> studentList) {
            super(context, textViewResourceId, studentList);
            this.studentList = new ArrayList<Student>();
            this.studentList.addAll(studentList);
        }

        private class ViewHolder {
            Button student;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final ViewHolder holder;
            Log.v("ConvertView", String.valueOf(position));

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.class_button, null);

                holder = new ViewHolder();
                holder.student = (Button) convertView.findViewById(R.id.class_button);
                convertView.setTag(holder);

                holder.student.setOnClickListener( new View.OnClickListener() {
                    public void onClick(View v) {

                        selectedStudent = holder.student.getText().toString();
                        selectedStudentID = selectedStudent.substring(selectedStudent.length() - 8);
                        Log.d(TAG, "Selected student: " + selectedStudentID);
                        Intent intent = new Intent(v.getContext(), Objectives.class);
                        startActivity(intent);
                    }
                });
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }

            Student currentStudent = studentList.get(position);
            holder.student.setText(currentStudent.getStudentFirstName() + " " + currentStudent.getStudentLastName() + " - " + currentStudent.getStudentZID());

            return convertView;

        }

    }

}
