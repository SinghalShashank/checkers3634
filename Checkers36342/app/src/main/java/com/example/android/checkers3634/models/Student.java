package com.example.android.checkers3634.models;

import com.example.android.checkers3634.models.Class;

import static android.R.attr.name;

/**
 * Created by Shashank on 22-Oct-17.
 */

public class Student extends Class {
    private String classID;
    private String studentFirstName;
    private String studentLastName;
    private String studentZID;
    public String getClassID() {
        return classID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }


    public Student(String firstName, String lastName, String zID, String classID){
        this.studentFirstName = firstName;
        this.studentLastName = lastName;
        this.studentZID = zID;
        this.classID = classID;
        this.studentProgress= 0;
    }



    public String getStudentFirstName() {
        return studentFirstName;
    }

    public void setStudentFirstName(String studentFirstName) {
        this.studentFirstName = studentFirstName;
    }

    public String getStudentLastName() {
        return studentLastName;
    }

    public void setStudentLastName(String studentLastName) {
        this.studentLastName = studentLastName;
    }

    private int studentProgress;

    public Student(){

    }

    public int getStudentProgress() {
        return studentProgress;
    }

    public void setStudentProgress(int studentProgress) {
        this.studentProgress = studentProgress;
    }

    public String getStudentZID() {

        return studentZID;
    }

    public void setStudentZID(String studentZID) {
        this.studentZID = studentZID;
    }




}
