package com.example.android.checkers3634;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.checkers3634.models.Objective;
import com.example.android.checkers3634.models.Student;
import com.example.android.checkers3634.models.StudentObjective;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static com.example.android.checkers3634.Classes.selectedClass;
import static com.example.android.checkers3634.R.string.username;
import static com.example.android.checkers3634.Students.selectedStudentID;


/**
 * Created by Shashank on 18-Oct-17.
 */

public class Objectives extends Activity {
    private Button mNotesButton;
    private static Objective currentObjective;
    public static ArrayList<Objective> objectiveList;
    public static String selectedStudent;

    public static Context mContext;
    private DatabaseReference mDatabase;
    private MyCustomAdapter dataAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.objectives);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("objectives");
        mContext = this;
        displayListView();

        mNotesButton = (Button) findViewById(R.id.notes_button);
        mNotesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), EditNote.class);
                startActivity(intent);
            }
        });

    }

    private void populateView() {
        dataAdapter = new MyCustomAdapter(mContext, R.layout.check_boxes, objectiveList);
        ListView listView = (ListView) findViewById(R.id.listView1);
        listView.setAdapter(dataAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Objective currObjective = (Objective) parent.getItemAtPosition(position);

            }
        });
    }

    private void saveObjective(String objectiveID, String objectiveName, boolean completed) {
        String key = mDatabase.child("objectives").push().getKey();
        StudentObjective obj = new StudentObjective(selectedStudentID, objectiveID, objectiveName, completed);
        Map<String, Object> objValues = obj.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/" + selectedStudentID + "/" + objectiveID, objValues);

        mDatabase.updateChildren(childUpdates);
    }

    private void displayListView() {
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                objectiveList = new ArrayList<Objective>();
                Log.d(TAG, dataSnapshot.getChildrenCount() + "");
                for (DataSnapshot snap : dataSnapshot.getChildren()) {

                    if (snap.hasChild("name")) {
                        String objectiveID = snap.getKey().toString();
                        String objectiveName = snap.child("name").getValue().toString();
                        Log.d(TAG, "Adding new objective: " + snap.getValue().toString());
                        objectiveList.add(new Objective(objectiveID, objectiveName, false));
                    }
                    if (snap.getKey().equalsIgnoreCase(Students.selectedStudentID)) {
                        for (DataSnapshot curObj : snap.getChildren()) {
                            Log.d(TAG, curObj.toString());
                            String currentObjID = curObj.getKey().toString();
                            if (curObj.hasChild("completed")) {
                                boolean isCompleted = (Boolean) curObj.child("completed").getValue();
                                Log.d(TAG, currentObjID + ": " + isCompleted);
                                String currentObjName = curObj.child("objective-name").getValue().toString();

                                objectiveList.set(Integer.parseInt(currentObjID) - 1 , new Objective(currentObjID, currentObjName, isCompleted));
                                Log.d(TAG, "Adding new objective: " + currentObjName);
                            }
                        }
                        continue;
                    }
                }
                populateView();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.d(TAG, "Failed to read value.", error.toException());
            }
        });

    }

    private class MyCustomAdapter extends ArrayAdapter<Objective> {

        private ArrayList<Objective> objectiveList;

        public MyCustomAdapter(Context context, int textViewResourceId,
                               ArrayList<Objective> objectiveList) {
            super(context, textViewResourceId, objectiveList);
            this.objectiveList = new ArrayList<Objective>();
            this.objectiveList.addAll(objectiveList);
        }

        private class ViewHolder {
            TextView code;
            CheckBox name;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            Log.v("ConvertView", String.valueOf(position));

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater) getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.check_boxes, null);

                holder = new ViewHolder();
                holder.code = (TextView) convertView.findViewById(R.id.code);
                holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
                convertView.setTag(holder);

                holder.name.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        CheckBox cb = (CheckBox) v;
                        Objective objective = (Objective) cb.getTag();
                        Toast.makeText(getApplicationContext(),
                                selectedStudentID + " has completed objective " + cb.getText(),
                                Toast.LENGTH_SHORT).show();
                        objective.setCompleted(true);
                        saveObjective(objective.getCode().toString(), cb.getText().toString(), cb.isChecked());
                    }
                });
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Objective objective = objectiveList.get(position);
            holder.code.setText(" (" + objective.getCode() + ")");
            holder.name.setText(objective.getName());
            holder.name.setChecked(objective.isCompleted());
            holder.name.setTag(objective);

            return convertView;

        }

    }
}