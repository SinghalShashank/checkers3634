package com.example.android.checkers3634;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import com.example.android.checkers3634.models.Class;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.android.checkers3634.models.Objective;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;


/**
 * Created by Shashank on 18-Oct-17.
 */

public class Classes extends AppCompatActivity /*implements LoaderManager.LoaderCallbacks<Cursor>*/
{
    public static Context mContext;

    public static String selectedClass;
    private DatabaseReference mDatabase;
    private static ArrayList<Class> classList;
    //private Button mClassButton;
    private MyCustomAdapter dataAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.classes);
        mContext = getApplicationContext();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("classes");
        loadClasses();
        /*mClassButton = (Button) findViewById(R.id.classButton);
        mClassButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(),Students.class);
                startActivity(intent);
            }
        });*/
    }

    private void loadClasses() {

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                classList = new ArrayList<Class>();
                Log.d(TAG, dataSnapshot.getChildrenCount() + "");
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    if (snap.hasChild("duration") && snap.hasChild("time") && snap.hasChild("day") && snap.hasChild("tutor-zid")) {
                        String classTutorzID = snap.child("tutor-zid").getValue().toString();
                        if (classTutorzID.equalsIgnoreCase(MainActivity.currentzID)) {
                            String classDuration = snap.child("duration").getValue().toString();
                            String classTime = snap.child("time").getValue().toString();
                            String classDay = snap.child("day").getValue().toString();
                            Log.d(TAG, "Adding new class: " + snap.getValue().toString());
                            classList.add(new Class(classDuration, classTime, classDay, classTutorzID));
                        }
                    }
                }
                populateView();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.d(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    private void populateView() {
        //create an ArrayAdaptar from the String Array
        dataAdapter = new MyCustomAdapter(mContext, R.layout.class_button, classList);
        ListView listView = (ListView) findViewById(R.id.classes_listview);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Class currClass = (Class) parent.getItemAtPosition(position);

            }
        });
    }


    private class MyCustomAdapter extends ArrayAdapter<Class> {

        private ArrayList<Class> classList;

        public MyCustomAdapter(Context context, int textViewResourceId,
                               ArrayList<Class> classList) {
            super(context, textViewResourceId, classList);
            this.classList = new ArrayList<Class>();
            this.classList.addAll(classList);
        }

        private class ViewHolder {
            Button classTime;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final ViewHolder holder;
            Log.v("ConvertView", String.valueOf(position));

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.class_button, null);

                holder = new ViewHolder();
                holder.classTime = (Button) convertView.findViewById(R.id.class_button);
                convertView.setTag(holder);

                holder.classTime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        selectedClass = holder.classTime.getText().toString();
                        Log.d(TAG, "Selected class: " + selectedClass);
                        Intent intent = new Intent(v.getContext(), Students.class);
                        startActivity(intent);
                    }
                });
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }

            Class currentClass = classList.get(position);
            holder.classTime.setText(currentClass.getDay() + " - " + currentClass.getTime());

            return convertView;

        }

    }

}

