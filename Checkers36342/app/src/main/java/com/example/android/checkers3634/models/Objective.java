package com.example.android.checkers3634.models;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

import static android.R.attr.author;

/**
 * Created by Shashank on 22-Oct-17.
 */

public class Objective {

    String id;
    String name;

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    boolean completed;

    public Objective(String code, String name, boolean completed) {
        this.id = code;
        this.name = name;
        this.completed = completed;
    }

    public String getCode() {
        return id;
    }
    public void setCode(String code) {
        this.id = code;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

}