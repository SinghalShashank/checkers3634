package com.example.android.checkers3634.models;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Shashank on 22-Oct-17.
 */

public class Note extends Student{
    private String noteID;
    private String content;


    public Note(){

    }

    public Note(String Content){
        this.content = Content;
    }


    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("content", content);
        return result;
    }
}