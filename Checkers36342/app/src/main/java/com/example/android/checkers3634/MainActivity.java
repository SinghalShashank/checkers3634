package com.example.android.checkers3634;

import android.app.Activity;
import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static com.loopj.android.http.AsyncHttpClient.LOG_TAG;

public class MainActivity extends AppCompatActivity {
    private static Context mContext;
    private Button mLoginButton;
    private EditText mzID;
    private EditText mzPass;
    public static String currentzID;
    private UNSWAPI zLogin;
    private String demozID = "z0000000";
    private String demozPass = "abc123";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = getApplicationContext();
        zLogin = new UNSWAPI("https://groupbot.us/api", "INFS3634", getApplicationContext());
        mzID = (EditText) findViewById(R.id.zid);
        mzPass = (EditText) findViewById(R.id.zpass);
        mLoginButton = (Button) findViewById(R.id.loginButton);

        mLoginButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String zID = mzID.getText().toString();
                String zPass = mzPass.getText().toString();
                zLogin.setzID(zID);
                zLogin.setzPass(zPass);
                if (!zLogin.validateLogin()) {
                    Toast.makeText(getApplicationContext(), "Please fill in your zID and zPass correctly!", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(getApplicationContext(), "Attempting to login...", Toast.LENGTH_LONG).show();

                // Demo account for z0000000
                if (zID.equalsIgnoreCase(demozID) && zPass.equals(demozPass)) {
                    currentzID = demozID;
                    handleLogin(true);
                    return;
                }

                try {
                    zLogin.tryLogin();
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public static void handleLogin(boolean success) {
        if (success) {

            Intent classes = new Intent(mContext, Classes.class);
            mContext.startActivity(classes);
        } else {
            Toast.makeText(mContext, "Incorrect login", Toast.LENGTH_SHORT).show();
        }
    }
}
