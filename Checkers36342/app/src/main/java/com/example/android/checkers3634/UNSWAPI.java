package com.example.android.checkers3634;

import android.content.Context;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.Base64;
import com.loopj.android.http.TextHttpResponseHandler;
import org.json.JSONObject;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;
import cz.msebera.android.httpclient.message.BasicHeader;

import static java.net.Proxy.Type.HTTP;

/**
 * The purpose of this API is to integrate MyUNSW login into Android apps
 *
 * Usage:
 *    # In your Android activity, declare an instance of this class
 *        UNSWAPI zLogin = new zLogin("url", "INFS3634", getApplicationContext());
 *
 *    # To try login, create a static void to handle the login status (when authenticated is set to true, the user is logged in)
 *        public static void handleLogin(boolean authenticated) { }
 *
 *    # To perform the login, set zID and zPass using setters
 *        zLogin.setzID("zID");
 *        zLogin.setzPass("zPass");
 *
 *    # To start the authentication process AFTER zID and zPass is set, encase the following in a try catch statement
 *        zLogin.tryLogin();
 *
 *    # To perform basic data validation, use the validateLogin boolean, which will return true if data is validated
 *        zLogin.validateLogin();
 *
 * Created by Andrew on 20/10/17.
 */

public class UNSWAPI {
    private String endpointURL;
    private String zID;
    private String zPass;
    private String APIkey;
    public static String responseResult;
    private Context context;

    public UNSWAPI(String endpointURL, String API_KEY, Context context) {
        this.endpointURL = endpointURL;
        this.APIkey = API_KEY;
        this.context = context;
    }

    public interface OnResponseCallback {
        // We create an interface to handle the async API request
        public void onResponse(boolean success);
    }

    private void onLogin(final OnResponseCallback callback) throws Exception {
        // Encode zPass for sanity
        String encodedPassword = Base64.encodeToString(getzPass().getBytes(), Base64.DEFAULT);
        encodedPassword = encodedPassword.replace("\n", "").replace("\r", "");

        // Create a new JSON payload for request
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("key", getAPIkey());
        jsonObject.put("zid", getzID());
        jsonObject.put("password", encodedPassword);

        // Convert payload to byte array entity for easy POSTing
        ByteArrayEntity entity = new ByteArrayEntity(jsonObject.toString().getBytes("UTF-8"));

        // Ensure the header is for application/json
        entity.setContentType(new BasicHeader(cz.msebera.android.httpclient.protocol.HTTP.CONTENT_TYPE, "application/json"));

        // Declare the REST HTTP client and set headers
        AsyncHttpClient client = new AsyncHttpClient();

        client.addHeader("Accept", "application/json");
        client.addHeader("Content-Type", "application/json");

        // Post the actual request, and get response
        client.post(getContext(), getEndpointURL(), entity, "application/json", new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                System.out.println("Failed: " + responseString);
                callback.onResponse(false);
                return;
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                // Upon request completion, store data statically and trigger the callback
                responseResult = responseString;
                System.out.println("Request returned: " + responseString);
                callback.onResponse(true);
                return;
            }
        });
    }


    public void tryLogin() throws Exception {
        OnResponseCallback loginCallback = new OnResponseCallback() {
            @Override
            public void onResponse(boolean success) {
            if (success) {
                boolean result = responseResult.equalsIgnoreCase(getzID());
                MainActivity.currentzID = getzID();
                MainActivity.handleLogin(result);
                return;
            }
            }
        };
        onLogin(loginCallback);
    }

    public Context getContext() {
        return context;
    }

    public boolean validateLogin() {
        if (getzID().length() != 8 || getzPass().length() < 1 || !getzID().startsWith("z")) {
            return false;
        }
        return true;
    }

    public String getAPIkey() {
        return APIkey;
    }

    public void setAPIkey(String APIkey) {
        this.APIkey = APIkey;
    }

    public String getEndpointURL() {
        return endpointURL;
    }

    public void setEndpointURL(String endpointURL) {
        this.endpointURL = endpointURL;
    }

    public String getzID() {
        return zID;
    }

    public void setzID(String zID) {
        this.zID = zID;
    }

    public String getzPass() {
        return zPass;
    }

    public void setzPass(String zPass) {
        this.zPass = zPass;
    }
}