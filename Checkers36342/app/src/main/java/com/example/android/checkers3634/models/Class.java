package com.example.android.checkers3634.models;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Shashank on 22-Oct-17.
 */

@IgnoreExtraProperties
public class Class {

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    private String duration;
    private String time;
    private String day;
    private String tutorZID;

    public Class (String duration, String time, String day, String tutorZID){
        this.duration = duration;
        this.time = time;
        this.day = day;
        this.tutorZID = tutorZID;
    }

    public String getTutorZID() {
        return tutorZID;
    }

    public void setTutorZID(String tutorZID) {
        this.tutorZID = tutorZID;
    }

    public String getTime() {

        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDay() {

        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Class() {

    }

}
