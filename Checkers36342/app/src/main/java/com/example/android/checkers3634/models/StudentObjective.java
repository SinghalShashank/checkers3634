package com.example.android.checkers3634.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

import static android.R.attr.author;

/**
 * Created by Andrew on 22/10/17.
 */

@IgnoreExtraProperties
public class StudentObjective {
    public String zid;
    public String objectiveID;
    public String objectiveName;
    public boolean completed;

    public StudentObjective() {
    }

    public StudentObjective(String zid, String objectiveID, String objectiveName, boolean completed) {
        this.zid = zid;
        this.objectiveID = objectiveID;
        this.objectiveName = objectiveName;
        this.completed = completed;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("zid", zid);
        result.put("objective-id", objectiveID);
        result.put("objective-name", objectiveName);
        result.put("completed", completed);

        return result;
    }
}
